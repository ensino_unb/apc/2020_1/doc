# Plano da Disciplina - Análise e Programação de Algoritmos (113476)

## Professores
* Fabricio Ataides Braz
* Gustavo Corrêia de Lima
* Nilton Correia da Silva

## Período
1º Semestre de 2.020

## Turmas
AA e BB

## Ementa
Princípios fundamentais de construção de programas. Construção de algoritmos e sua representação em pseudocódigo e linguagens de alto nível. Noções de abstração. Especificação de variáveis e funções. Testes e depuração. Padrões de soluções em programação. Noções de programação estruturada. Identificadores e tipos. Operadores e expressões. Estruturas de controle: condicional e repetição. Entrada e saída de dados. Estruturas de dados estáticas: agregados homogêneos e heterogêneos. Iteração e recursão. Noções de análise de custo e complexidade. Desenvolvimento sistemático e implementação de programas. Estruturação, depuração, testes e documentação de programas. Resolução de problemas. Aplicações em casos reais e questões ambientais.

## Método

Independente do método de ensino, a programação de computador é uma habilidade, cuja apreensão exige experimentos contínuos de exercício dos fundamentos da programação. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. Neste semestre decidimos aplicar o método de aprendizado baseado em projeto. 

Uma das principais mudanças que aprendizado baseado em projeto traz é que o foco sai da instrução, em que o professor em sala de aula instrui o aluno sobre conceitos e ferramentas, para a investigação, em que o aluno é desafiado a pesquisar conceitos, técnicas e ferramentas para conseguir alcançar os objetivos do projeto que ele se comprometeu a desenvolver. A perspectiva do professor muda da instrução, para a tutoria, no que diz respeito ao ensino. A perspectiva do aluno muda de passiva para ativa, no que diz respeito ao aprendizado.

A disciplina prevê um total de 90 horas de formação, a serem distribuídas da seguinte maneira:

| Horas | Atividade                          | Natureza |
|-------|------------------------------|---------|
| 18   | Acolhimento e nivelamento              | Mista |
| 21   | Tutorias                                | Sincrona |
| 38   | Atividades em Grupo                    | Assincrona |
| 1    | Planejamento                           | Assincrona |
| 12    | Seminários                           | Sincrona |

Serão constituídos grupos em que o aluno, apoioado por até 20 (vinte) tutores (professores e monitores), percorrerá uma trilha de aprendizagem voltada para a construção de dashboards.

## Ferramentas & Materiais
* [Teams](https://teams.microsoft.com/l/channel/19%3a998cc63b273043968bc650046b742c3d%40thread.tacv2/General?groupId=11979583-1aaa-40f3-a4a1-fae8a89b6c4f&tenantId=ec359ba1-630b-4d2b-b833-c8e6d48f8059) - Comunicação e trabalho colaborativo
* [Trello](https://trello.com/b/9abnJY4R/project-based-learning-apc) - gerência do projeto
* [Python](https://www.python.org/) - Linguagem de programação
* [Plotly](https://plotly.com/) - Biblioteca de construção de dashboard
* [Gitlab](https://gitlab.com/ensino_unb/apc/2020_1/doc) - Reposotório de código e coloboração


## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir desempenho superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Desempenho

A avaliação de desempenho é resultado da avaliação pelos professores do resultado do grupo, dado o projeto, juntamente com a avaliação individual do aluno pelos membros do grupo.

* AGP: avaliação do grupo pelos professores. Esta avaliação acontece de acordo com os marcos estabelecidos no template de projeto do Trello compatilhado. No dia da apresentação será sorteado um membro do grupo que deverá ter a capacidade de responder pelo projeto.

* AIG: avaliação individual pelos membros do grupo.

![equation](https://latex.codecogs.com/gif.latex?%5Cdfrac%7B%7B%5Csqrt%7B%7BAGP_1%2AAIG_1%7D%7D%7D%2B%202%2A%7B%5Csqrt%7B%7BAGP_2%2AAIG_2%7D%7D%7D%2B%7B%5Csqrt%7B%7BAGP_3%2AAIG_3%7D%7D%7D%7D%7B4%7D)

Serão três encontros avaliativos, observando os seguintes apectos do projeto:
1. Design (peso 1)
2. Desenvolvimento (peso 2)
3. Lançamento (peso 1)

As fases e datas encontram-se detalhadas no Trello. 

### Comparecimento

As atividades síncronas serão consideradas para contabilizar a presença.

## Dinâmica no Grupo
Os grupos serão formados com a quantidade 7 alunos. Em cada iteração, ou seja, períodos entre os encontros avaliativos, deverão ser designados no grupo o `líder`, responsável por conduzir o time na execução das metas do período, além de reportar aos professores desvios e problemas de seus membros.

### Equilíbrio
Para que o grupo não seja prejudicado por eventuais desvios de membros do seu grupo, os alunos que não alcancarem nota igual ou superior a 5 na AIG serão desprezados do sorteio para apresentação.

### Evasão
Grupos com menos de 4 alunos, terão seus membros distribuidos em outros grupos.

## Referências Bibliográficas

* Básica 
    * Cormen, T. et al., Algoritmos: Teoria e Prática. 3a ed., Elsevier - Campus, Rio de Janeiro, 2012 
    * Ziviani, N., Projeto de Algoritmos com implementação em Pascal e C, 3a ed., Cengage Learning, 2010. 
Felleisen, M. et al., How to design programs: an introduction to computing and programming, MIT Press, EUA, 2001. 

* Complementar 
    * Evans, D., Introduction to Computing: explorations in Language, Logic, and Machi nes, CreatSpace, 2011. 
    * Harel, D., Algorithmics: the spirit of computing, Addison-Wesley, 1978. 
    * Manber, U., Introduction to algorithms: a creative approach, Addison-Wesley, 1989. 
    * Kernighan, Brian W; Ritchie, Dennis M.,. C, a linguagem de programacao: Padrao ansi. Rio de janeiro: Campus 
    * Farrer, Harry. Programação estruturada de computadores: algoritmos estruturados. Rio de Janeiro: Guanabara Dois, 2002.
    * [Plotly](https://dash-gallery.plotly.host/Portal)
    * [Portal Brasileiro de Dados Abertos](http://dados.gov.br)
    * [Kaggle](http://kaggle.com/)
    * [Ambiente de Desenvolvimento Python](https://realpython.com/installing-python/)
    * [Python Basics](https://www.learnpython.org)

* Cursos de python
    - Os alunos podem escolher qualquer fonte extra de aprendendizagem para a linguagem Python. Os recursos são enormes. Algumas sugestões iniciais são:
    * https://www.pycursos.com/python-para-zumbis/
    * https://www.youtube.com/playlist?list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn
    * https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6
    * https://www.youtube.com/watch?v=O2xKiMl-d7Y&list=PL70CUfm2J_8SXFHovpVUbq8lB2JSuUXgk
    * https://solyd.com.br/treinamentos/python-basico/
    * https://wiki.python.org/moin/BeginnersGuide/NonProgrammers
